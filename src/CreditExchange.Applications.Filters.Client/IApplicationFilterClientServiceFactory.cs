﻿using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Security.Tokens;

namespace CreditExchange.Applications.Filters.Client
{
    public interface IApplicationFilterClientServiceFactory
    {
        IApplicationFilterService Create(ITokenReader reader);
    }
}

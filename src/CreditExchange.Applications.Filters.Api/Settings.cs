﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace CreditExchange.Applications.Filters.Api
{
    public class Settings
    {
        public static string ServiceName { get; } = "application-filters";

        public static string TaggingEvents { get; } = "tagging-events";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings DecisionEngine { get; } = new ServiceSettings($"{Prefix}_DECISION_ENGINE", "decision-engine");

        public static ServiceSettings Application { get; } = new ServiceSettings($"{Prefix}_APPLICATION", "application");

        public static ServiceSettings Applicant { get; } = new ServiceSettings($"{Prefix}_APPLICANT", "applicant");

        public static ServiceSettings StatusManagement { get; } = new ServiceSettings($"{Prefix}_STATUS_MANAGEMENT", "status-management");

        public static ServiceSettings OfferEngine { get; } = new ServiceSettings($"{Prefix}_OFFER_ENGINE", "offer-engine");

        public static ServiceSettings VerificationEngine { get; } = new ServiceSettings($"{Prefix}_VERIFICATION_ENGINE", "verification-engine");

        public static ServiceSettings DataAttributes { get; } = new ServiceSettings($"{Prefix}_DATA_ATTRIBUTES","data-attributes");

        public static ServiceSettings Assignment { get; } = new ServiceSettings($"{Prefix}_ASSIGNMENT", "assignment");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";

        public static ServiceSettings Identity { get; } = new ServiceSettings($"{Prefix}_IDENTITY", "identity");
        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", ServiceName);
    }
}

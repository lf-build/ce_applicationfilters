﻿﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
using LendFoundry.EventHub;
using LendFoundry.EventHub.Client;
using CreditExchange.Application.Client;
using CreditExchange.Applications.Filters.Persistence;
using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement.Client;
using LendFoundry.DataAttributes.Client;
using LendFoundry.VerificationEngine.Client;
using LendFoundry.Tenant.Client;
using LendFoundry.Clients.DecisionEngine;
using CreditExchange.Applicant.Client;
using CreditExchange.Applications.Filters.Abstractions.Configurations;
using CreditExchange.Applications.Filters.Abstractions.Services;
using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.ApplicationProcessor.Client;
using System;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.Configuration;

namespace CreditExchange.Applications.Filters.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
           
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "ApplicationsFilters"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "CreditExchange.Applications.Filters.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();


            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<Configuration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddConfigurationService<TaggingConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.TaggingEvents);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddApplicationService(Settings.Application.Host, Settings.Application.Port);
            services.AddApplicantService(Settings.Applicant.Host, Settings.Applicant.Port);
            services.AddDecisionEngine(Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddStatusManagementService(Settings.StatusManagement.Host, Settings.StatusManagement.Port);
            services.AddOfferEngineService(Settings.OfferEngine.Host, Settings.OfferEngine.Port);
            services.AddVerificationEngineService(Settings.VerificationEngine.Host, Settings.VerificationEngine.Port);
            services.AddDataAttributes(Settings.DataAttributes.Host, Settings.DataAttributes.Port);
            services.AddAssignmentService(Settings.Assignment.Host, Settings.Assignment.Port);
            services.AddIdentityService(Settings.Identity.Host, Settings.Identity.Port);
            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<Configuration>>().Get());
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<TaggingConfiguration>>().Get());
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IApplicationFilterService, ApplicationFilterService>();
            services.AddTransient<IFilterViewRepository, FilterViewRepository>();
            services.AddTransient<IFilterViewRepositoryFactory, FilterViewRepositoryFactory>();
            services.AddTransient<IFilterViewTagsRepository, FilterViewTagsRepository>();
            services.AddTransient<IFilterViewTagsRepositoryFactory, FilterViewTagsRepositoryFactory>();
            services.AddTransient<IApplicationFilterListener, ApplicationFilterListener>();
            services.AddTransient<IApplicationFilterServiceFactory, ApplicationFilterServiceFactory>();
            services.AddTransient<ICsvGenerator, CsvExporter.CsvGenerator>();


            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
           
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "ApplicationsFilters Service");
            });
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            //app.UseEventHub();
            try
            {
                app.UseApplicationFilterListener();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            app.UseMvc();
            app.UseHealthCheck();
        }
    }
}

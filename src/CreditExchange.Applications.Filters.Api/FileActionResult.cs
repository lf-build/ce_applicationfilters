﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace CreditExchange.Applications.Filters.Api
{
    public class FileActionResult : ActionResult
    {
        public FileActionResult(string fileDownloadName, byte[] content, string contentType)
        {
            FileDownloadName = fileDownloadName;
            Content = content;
            ContentType = contentType;
        }

        public string ContentType { get; }
        public string FileDownloadName { get; }
        public byte[] Content { get; }

        public override async Task ExecuteResultAsync(ActionContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = ContentType;
            response.ContentLength = Content.Length;
            response.Headers.Add("Content-Disposition", new[] { "attachment; filename=" + FileDownloadName });

            using (var stream = new MemoryStream(Content))
            {
                await stream.CopyToAsync(context.HttpContext.Response.Body);
            }
        }
    }
}
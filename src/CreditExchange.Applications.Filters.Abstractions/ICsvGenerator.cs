﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters
{
    public interface ICsvGenerator
    {
        string Delimiter { get; set; }
        byte[] WriteToCsv<T>(IEnumerable<T> items, Dictionary<string, string> csvProjection);
    }
}

﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace CreditExchange.Applications.Filters.Abstractions
{
    public interface IFilterViewTags : IAggregate
    {
        string ApplicationNumber { get; set; }
        IEnumerable<TagInfo> Tags { get; set; }
    }
}

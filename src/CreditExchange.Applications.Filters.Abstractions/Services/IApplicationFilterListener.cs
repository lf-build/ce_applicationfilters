﻿namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterListener
    {
        void Start();
    }
}

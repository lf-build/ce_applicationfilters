﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public class FundingData : IFundingData
    {
       public double FundedAmount { get; set; }
      public  DateTimeOffset FundedDate { get; set; }
      public  double EmiAmount { get; set; }
      public  DateTimeOffset FirstEmiDate { get; set; }
    }
}

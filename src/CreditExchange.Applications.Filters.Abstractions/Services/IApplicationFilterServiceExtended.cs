﻿﻿
using LendFoundry.EventHub;

namespace CreditExchange.Applications.Filters.Abstractions.Services
{
    public interface IApplicationFilterServiceExtended : IApplicationFilterService
    {
        void ProcessTaggingEvent(EventInfo @event);
    }
}

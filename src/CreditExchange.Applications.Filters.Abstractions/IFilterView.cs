﻿using System;
using System.Collections.Generic;
using CreditExchange.Applications.Filters.Abstractions;
using LendFoundry.Foundation.Persistence;

namespace CreditExchange.Applications.Filters
{
    public interface IFilterView : IAggregate
    {
        string ApplicationId { get; set; }
        string ApplicantId { get; set; }
        string ApplicationNumber { get; set; }
        string Applicant { get; set; }
        double AmountRequested { get; set; }
        string RequestTermType { get; set; }
        double RequestTermValue { get; set; }
        string ApplicantFirstName { get; set; }
        string ApplicantLastName { get; set; }
        string ApplicantMiddleName { get; set; }
        DateTimeOffset? ApplicantDateOfBirth { get; set; }
        string ApplicantPhone { get; set; }
        string ApplicantWorkEmail { get; set; }
        string ApplicantPersonalEmail { get; set; }
        string ApplicantPanNumber { get; set; }
        string ApplicantAadharNumber { get; set; }
        string ApplicantEmployer { get; set; }
        string ApplicantAddressLine1 { get; set; }
        string ApplicantAddressLine2 { get; set; }
        string ApplicantAddressLine3 { get; set; }
        string ApplicantAddressLine4 { get; set; }
        string ApplicantAddressCity { get; set; }
        string ApplicantAddressCountry { get; set; }
        bool ApplicantAddressIsDefault { get; set; }
        DateTimeOffset Submitted { get; set; }
        DateTimeOffset ExpirationDate { get; set; }
        DateTimeOffset LastProgressDate { get; set; }

        string StatusCode { get; set; }
        string StatusName { get; set; }
        DateTimeOffset StatusDate { get; set; }
        List<string> StatusReasons { get; set; }

        string SourceId { get; set; }
        string SourceType { get; set; }

        string FileType { get; set; }
        double InitialOfferAmount { get; set; }
        double InitialOfferInterestRate { get; set; }
        int InitialOfferLoanTenure { get; set; }
        double InitialOfferScore { get; set; }
        DateTimeOffset? InitialOfferSelectedOn { get; set; }

        double FinalOfferAmount { get; set; }
        double FinalOfferInterestRate { get; set; }
        double FinalOfferInstallmentAmount { get; set; }
        string FinalOfferLoanTenure { get; set; }
        double FinalOfferProcessingFee { get; set; }
        DateTimeOffset? FinalOfferSelectedOn { get; set; }

        string FinacleDocumentId { get; set; }
        string FinacleDocEntityId { get; set; }

        string HunterDocumentId { get; set; }
        string HunterDocEntityId { get; set; }

        DateTime? CanReapplyBy { get; set; }
        Dictionary<string, object> StatusHistory { get; set; }

        DateTimeOffset? OfferRejectedDate { get; set; }
        DateTimeOffset? OfferExpiredDate { get; set; }
        DateTimeOffset? ApprovedDate { get; set; }
        DateTimeOffset? NotInterestedDate { get; set; }
        Dictionary<string, string> Assignees { get; set; }
        string PromoCode { get; set; }
        string TrackingCode { get; set; }
        bool? HunterStatus { get; set; }
        string SodexoCode { get; set; }
        string TrackingCodeMedium { get; set; }
        string HunterId { get; set; }
        string SchemeCode { get; set; }
        double? Income { get; set; }
        string ApplicantAddressPincode { get; set; }

        IEnumerable<TagInfo> Tags { get; set; }

        string LeadSquaredCRMId { get; set; }
        string SystemChannel { get; set; }
        string GCLId { get; set; }
        string LCaseApplicant { get; set; }
        string LCaseApplicantEmployer { get; set; }
        string LCaseApplicantPersonalEmail { get; set; }
        string LCaseApplicantPanNumber { get; set; }
        string TrackingCodeCampaign { get; set; }
        string TrackingCodeTerm { get; set; }
        string TrackingCodeContent { get; set; }
    }
}
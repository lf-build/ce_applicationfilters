﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Abstractions
{
    public class ApplicationSearchParams
    {
        public string FromApplicationDate { get; set; }
        public string ToApplicationDate { get; set; }
        public string FromExpiryDate { get; set; }
        public string ToExpiryDate { get; set; }
        public string DateOfBirth { get; set; }
        public string MobileNumber { get; set; }
        public string Name { get; set; }
        public string PanNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmployerName { get; set; }
        public string StatusCode { get; set; }
        public IEnumerable<string> InTags { get; set; }
        public IEnumerable<string> NotInTags { get; set; }
        public string ApplicationNumber { get; set; }
    }
}

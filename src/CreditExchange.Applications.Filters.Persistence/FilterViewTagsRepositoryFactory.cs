﻿using System;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;


namespace CreditExchange.Applications.Filters.Persistence
{
    public class FilterViewTagsRepositoryFactory : IFilterViewTagsRepositoryFactory
    {
        public FilterViewTagsRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewTagsRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new FilterViewTagsRepository(tenantService, mongoConfiguration);
        }
    }
}

﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using CreditExchange.Applications.Filters.Abstractions.Services;

namespace CreditExchange.Applications.Filters.Persistence
{
    public class FilterViewRepositoryFactory : IFilterViewRepositoryFactory
    {
        public FilterViewRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new FilterViewRepository(tenantService, mongoConfiguration);
        }
    }
}

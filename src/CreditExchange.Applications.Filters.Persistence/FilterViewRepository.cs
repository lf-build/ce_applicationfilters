﻿using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CreditExchange.Applications.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {
            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Submitted).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.Assignees)
                 .SetSerializer(new DictionaryInterfaceImplementerSerializer<Dictionary<string, string>>
                     (
                         DictionaryRepresentation.ArrayOfDocuments
                     )
                 );
                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "application-filters")
        {
            CreateIndexIfNotExists("applicant", Builders<IFilterView>.IndexKeys.Ascending(i => i.Applicant));
            CreateIndexIfNotExists("status-code", Builders<IFilterView>.IndexKeys.Ascending(i => i.StatusCode));
            CreateIndexIfNotExists("status-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.StatusName));
            CreateIndexIfNotExists("first-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantFirstName));
            CreateIndexIfNotExists("last-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantLastName));
            CreateIndexIfNotExists("middle-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantMiddleName));
            CreateIndexIfNotExists("mobileno", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantPhone));
            CreateIndexIfNotExists("pan", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantPanNumber));
            CreateIndexIfNotExists("personal-email", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantPersonalEmail));
            CreateIndexIfNotExists("employer", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicantEmployer));
            CreateIndexIfNotExists("application-date", Builders<IFilterView>.IndexKeys.Ascending(i => i.Submitted));
            CreateIndexIfNotExists("expiry-date", Builders<IFilterView>.IndexKeys.Ascending(i => i.ExpirationDate));
            CreateIndexIfNotExists("source-type", Builders<IFilterView>.IndexKeys.Ascending(i => i.SourceType));
            CreateIndexIfNotExists("source-id", Builders<IFilterView>.IndexKeys.Ascending(i => i.SourceId));
            CreateIndexIfNotExists("application-number", Builders<IFilterView>.IndexKeys.Ascending(i => i.ApplicationNumber));
            CreateIndexIfNotExists("submitted", Builders<IFilterView>.IndexKeys.Ascending(i => i.Submitted));
            CreateIndexIfNotExists("LCaseApplicant", Builders<IFilterView>.IndexKeys.Ascending(i => i.LCaseApplicant));
            CreateIndexIfNotExists("LCaseApplicantPanNumber", Builders<IFilterView>.IndexKeys.Ascending(i => i.LCaseApplicantPanNumber));
            CreateIndexIfNotExists("LCaseApplicantEmployer", Builders<IFilterView>.IndexKeys.Ascending(i => i.LCaseApplicantEmployer));
            CreateIndexIfNotExists("LCaseApplicantPersonalEmail", Builders<IFilterView>.IndexKeys.Ascending(i => i.LCaseApplicantPersonalEmail));

            var tenantPerEntity = Builders<IFilterView>.IndexKeys
                .Ascending(a => a.TenantId)
                .Ascending(a => a.StatusCode)
                .Ascending(a => a.ApplicationNumber)
                .Ascending(a => a.Applicant)
                .Ascending(a => a.ApplicantPhone)
                .Ascending(a => a.ApplicantPersonalEmail);

            CreateIndexIfNotExists(nameof(tenantPerEntity), tenantPerEntity, false);

            var applicationNumberPerEntity = Builders<IFilterView>.IndexKeys
                .Ascending(a => a.TenantId)
                .Ascending(a => a.StatusCode)
                .Ascending(a => a.ApplicantPanNumber)
                .Ascending(a => a.OfferRejectedDate);

            CreateIndexIfNotExists(nameof(applicationNumberPerEntity), applicationNumberPerEntity, false);

            var applicantperEntity = Builders<IFilterView>.IndexKeys
                .Ascending(a => a.TenantId)
                .Ascending(a => a.Applicant)
                .Ascending(a => a.ApplicationNumber)
                .Ascending(a => a.ApplicantPhone)
                .Ascending(a => a.ApplicantPanNumber)
                .Ascending(a => a.ApplicantPersonalEmail);

            CreateIndexIfNotExists(nameof(applicantperEntity), applicantperEntity, false);


        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.ApplicationNumber == view.ApplicationNumber;

            var existingView = Query
                .Where(v => v.ApplicationNumber == view.ApplicationNumber && v.TenantId == TenantService.Current.Id)
                .FirstOrDefault();

            view.TenantId = TenantService.Current.Id;
            if (existingView != null && !string.IsNullOrEmpty(existingView.Id))
            {
                view.Id = existingView.Id;
                if (existingView.InitialOfferSelectedOn != null) { view.InitialOfferSelectedOn = existingView.InitialOfferSelectedOn; }
                if (existingView.FinalOfferSelectedOn != null) { view.FinalOfferSelectedOn = existingView.FinalOfferSelectedOn; }
            }
            else
                view.Id = ObjectId.GenerateNewId().ToString();

            Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
        }

        public IEnumerable<IFilterView> GetAll()
        {
            return OrderIt(Query);
        }

        public async Task<IDictionary<string, int>> GetReportStatusCount()
        {
            DateTime now = DateTime.UtcNow;
            DateTime dtmin = now.AddHours(-24);
            DateTime dtmax = now;

            return await Task.Run(() => (Query.Where(x => x.Submitted >= dtmin && x.Submitted <= dtmax).GroupBy(x => x.StatusName)
                      .Select(g => new { g.Key, Count = g.Count() }).ToDictionary(x => x.Key, x => x.Count)));
        }

        public IEnumerable<IFilterView> GetUTMTagReport(IEnumerable<string> applicationnumber)
        {
            return OrderIt(Query.Where(x => applicationnumber.Contains(x.ApplicationNumber)).Select(x => new FilterView { ApplicationNumber = x.ApplicationNumber, Applicant = x.Applicant, InitialOfferAmount = x.InitialOfferAmount, FinalOfferAmount = x.FinalOfferAmount, AmountRequested = x.AmountRequested, StatusName = x.StatusName, TrackingCode = x.TrackingCode }));
        }

        public IEnumerable<IFilterView> GetByStatus(IEnumerable<string> statuses)
        {
            return OrderIt(Query.Where(x => statuses.Contains(x.StatusCode)));
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId));
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId && statuses.Contains(x.StatusCode)));
        }

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.StatusCode)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<int> GetTotalSubmittedForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentMonth
                ));
        }

        public Task<int> GetTotalSubmittedForCurrentYear(string sourceType, string sourceId, DateTimeOffset currentYear)
        {
            return Task.FromResult(Query.Count
                (
                    app => app.SourceType.ToLower() == sourceType.ToLower() &&
                    app.SourceId == sourceId &&
                    app.Submitted >= currentYear
                ));
        }

        public Task<IEnumerable<IFilterView>> GetByName(string sourceType, string sourceId, string name)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(
                OrderIt(
                    Query.Where(a => a.SourceType.ToLower() == sourceType.ToLower()
                                    && a.SourceId == sourceId
                                    && a.Applicant.ToLower().Contains(name.ToLower()))));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode)))
            );
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(Query.Where(x => statuses.Contains(x.StatusCode)))
            );
        }

        public Task<int> GetCountByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<int>
            (
                Query.Count(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId &&
                                         statuses.Contains(x.StatusCode))
            );
        }

        //public Task<IEnumerable<IFilterView>> GetByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        //{
        //    return Task.FromResult<IEnumerable<IFilterView>>
        //    (
        //        OrderIt(from db in Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
        //                from dbTags in db.Tags
        //                where (from tag in tags select tag).Contains(dbTags)
        //                select db).Distinct()
        //    );
        //}

        //public Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags)
        //{
        //    return Task.FromResult<IEnumerable<IFilterView>>
        //    (
        //        OrderIt(from db in Query.ToList()
        //                from dbTags in db.Tags
        //                where (from tag in tags select tag).Contains(dbTags)
        //                select db).Distinct()
        //    );
        //}

        //public Task<int> GetCountByTag(string sourceType, string sourceId, IEnumerable<string> tags)
        //{
        //    return Task.FromResult<int>
        //    (
        //        (from db in Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId).ToList()
        //         from dbTags in db.Tags
        //         where (from tag in tags select tag).Contains(dbTags)
        //         select db).Distinct().Count()
        //    );
        //}

        public double GetTotalAmountApprovedBySourceAndStatus(string sourceType, string sourceId, string[] statuses)
        {
            return Query.Where(x => x.SourceType == sourceType && x.SourceId == sourceId && statuses.Contains(x.StatusCode))
                        .Sum(x => x.InitialOfferAmount);
        }

        public Task<IFilterView> GetByApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return Task.FromResult
                (
                    Query.FirstOrDefault(a => a.SourceType.ToLower() == sourceType.ToLower() &&
                        a.SourceId == sourceId &&
                        a.ApplicationNumber == applicationNumber)
                );
        }

        public Task<IFilterView> GetByApplicationNumber(string applicationNumber)
        {
            return Task.FromResult
                (
                    Query.FirstOrDefault(a => a.ApplicationNumber == applicationNumber)
                );
        }

        public Task<IEnumerable<IFilterView>> GetUsingUniqueAttributes(UniqueParameterTypes parameterType, string parameterValue)
        {
            Task<IEnumerable<IFilterView>> returnVal = null;
            switch (parameterType)
            {
                case UniqueParameterTypes.Email:
                    returnVal = Task.FromResult(Query.Where(a => a.ApplicantPersonalEmail.ToLower() == parameterValue.ToLower())?.OrderByDescending(x => x.Submitted)?.ToEnumerable());
                    break;
                case UniqueParameterTypes.Mobile:
                    returnVal = Task.FromResult(Query.Where(a => a.ApplicantPhone == parameterValue)?.OrderByDescending(x => x.Submitted)?.ToEnumerable());
                    break;
                case UniqueParameterTypes.Pan:
                    returnVal = Task.FromResult(Query.Where(a => a.ApplicantPanNumber.ToUpper() == parameterValue.ToUpper())?.OrderByDescending(x => x.Submitted)?.ToEnumerable());
                    break;
            }
            return returnVal;
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate)
        {
            return await Task.FromResult
           (
              Query.Where(p => p.ExpirationDate < todayDate).ToList()
           );
            //return await Task.FromResult
            //(
            //   Collection.Find(Builders<IFilterView>.Filter.Lt("ExpirationDate.0", todayDate.Ticks)).ToList()
            //);
        }

        public IEnumerable<IFilterView> SearchApplications(ApplicationSearchParams searchParams, string condition = null)
        {
            var filters = new List<FilterDefinition<IFilterView>>();

            if (!string.IsNullOrEmpty(searchParams.FromApplicationDate) && !string.IsNullOrEmpty(searchParams.ToApplicationDate))
            {
                var fromDate = DateTime.Parse(searchParams.FromApplicationDate);
                var toDate = DateTime.Parse(searchParams.ToApplicationDate);
                var appFilter = Builders<IFilterView>.Filter.Where(x => x.Submitted.DateTime.CompareTo(fromDate) >= 0 && x.Submitted.DateTime.CompareTo(toDate) <= 0);
                filters.Add(appFilter);
            }
            else if (!string.IsNullOrEmpty(searchParams.FromApplicationDate))
            {
                var fromDate = DateTime.Parse(searchParams.FromApplicationDate);
                var appFilter = Builders<IFilterView>.Filter.Where(x => x.Submitted.DateTime.CompareTo(fromDate) >= 0);
                filters.Add(appFilter);
            }
            else if (!string.IsNullOrEmpty(searchParams.ToApplicationDate))
            {
                var toDate = DateTime.Parse(searchParams.ToApplicationDate);
                var appFilter = Builders<IFilterView>.Filter.Where(x => x.Submitted.DateTime.CompareTo(toDate) <= 0);
                filters.Add(appFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.FromExpiryDate) && !string.IsNullOrEmpty(searchParams.ToExpiryDate))
            {
                var fromDate = DateTime.Parse(searchParams.FromExpiryDate);
                var toDate = DateTime.Parse(searchParams.ToExpiryDate);
                var fromExpirationFilter = Builders<IFilterView>.Filter.Gte("ExpirationDate.0", fromDate.Ticks);
                var toExpirationFilter = Builders<IFilterView>.Filter.Lte("ExpirationDate.0", toDate.Ticks);
                var appFilter = Builders<IFilterView>.Filter.And(fromExpirationFilter, toExpirationFilter);
                filters.Add(appFilter);
            }
            else if (!string.IsNullOrEmpty(searchParams.FromExpiryDate))
            {
                var fromDate = DateTime.Parse(searchParams.FromExpiryDate);
                var appFilter = Builders<IFilterView>.Filter.Gte("ExpirationDate.0", fromDate.Ticks);
                filters.Add(appFilter);
            }
            else if (!string.IsNullOrEmpty(searchParams.ToExpiryDate))
            {
                var toDate = DateTime.Parse(searchParams.ToExpiryDate);
                var appFilter = Builders<IFilterView>.Filter.Lte("ExpirationDate.0", toDate.Ticks);
                filters.Add(appFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.DateOfBirth))
            {
                var dob = DateTime.Parse(searchParams.DateOfBirth);
                var dobFilter = Builders<IFilterView>.Filter.Eq("ApplicantDateOfBirth.0", dob.Ticks);
                filters.Add(dobFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.MobileNumber))
            {
                var expression = new BsonRegularExpression("/^" + searchParams.MobileNumber + "/");
                var mobileFilter = Builders<IFilterView>.Filter.Regex("ApplicantPhone", expression);
                filters.Add(mobileFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.Name))
            {
                var expression = new BsonRegularExpression("/" + searchParams.Name + "/i");
                var nameFilter = Builders<IFilterView>.Filter.Regex("LCaseApplicant", expression);
                filters.Add(nameFilter);
                //var nameFilter1 = Builders<IFilterView>.Filter.Regex("ApplicantMiddleName", expression);
                //filters.Add(nameFilter1);
                //var nameFilter2 = Builders<IFilterView>.Filter.Regex("ApplicantLastName", expression);
                //filters.Add(nameFilter2);
            }

            if (!string.IsNullOrEmpty(searchParams.PanNumber))
            {
                var expression = new BsonRegularExpression("/" + searchParams.PanNumber + "/i");
                var panFilter = Builders<IFilterView>.Filter.Regex("LCaseApplicantPanNumber", expression);
                filters.Add(panFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.PersonalEmail))
            {
                var expression = new BsonRegularExpression("/^" + searchParams.PersonalEmail + "/i");
                var emailFilter = Builders<IFilterView>.Filter.Regex("LCaseApplicantPersonalEmail", expression);
                filters.Add(emailFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.EmployerName))
            {
                var expression = new BsonRegularExpression("/^" + searchParams.EmployerName + "/i");
                var employerFilter = Builders<IFilterView>.Filter.Regex("LCaseApplicantEmployer", expression);
                filters.Add(employerFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.StatusCode))
            {
                var statuslist = searchParams.StatusCode.Split(',').ToList();
                var statusFilter = Builders<IFilterView>.Filter.In("StatusCode", statuslist);
                filters.Add(statusFilter);
            }

            if (!string.IsNullOrEmpty(searchParams.ApplicationNumber))
            {
                var applicationNumberFilter = Builders<IFilterView>.Filter.Eq("ApplicationNumber", searchParams.ApplicationNumber);
                filters.Add(applicationNumberFilter);
            }
            if (filters.Count > 0)
            {
                FilterDefinition<IFilterView> finalFilter = null;
                if (!string.IsNullOrEmpty(condition) && condition.Equals("and", StringComparison.OrdinalIgnoreCase))
                {
                    finalFilter = Builders<IFilterView>.Filter.And(filters);
                }
                else
                {
                    finalFilter = Builders<IFilterView>.Filter.Or(filters);
                }
                return Collection.Find(finalFilter).ToEnumerable<IFilterView>().Where(x => x.TenantId == TenantService.Current.Id);
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<IFilterView> SearchApplicationForAttribute(string attributeName, string attributeValue)
        {
            System.Reflection.PropertyInfo prop = typeof(IFilterView).GetProperty(attributeName);
            return Collection.Find(Builders<IFilterView>.Filter.Eq(attributeName, attributeValue)).ToEnumerable<IFilterView>();
        }

        public void AttachFinacleDocumentId(string applicationNumber, string docManagerEntityId, string documentId)
        {
            var tenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
                        s.ApplicationNumber == applicationNumber &&
                        s.TenantId == tenantId,
                        new UpdateDefinitionBuilder<IFilterView>()
                            .Set(x => x.FinacleDocumentId, documentId)
                            .Set(x => x.FinacleDocEntityId, docManagerEntityId)
                    , new UpdateOptions() { IsUpsert = true });
        }

        public void AttachHunterDocumentId(string applicationNumber, string docManagerEntityId, string documentId)
        {
            var tenantId = TenantService.Current.Id;

            Collection.UpdateOne(s =>
                        s.ApplicationNumber == applicationNumber &&
                        s.TenantId == tenantId,
                        new UpdateDefinitionBuilder<IFilterView>()
                            .Set(x => x.HunterDocumentId, documentId)
                            .Set(x => x.HunterDocEntityId, docManagerEntityId)
                    , new UpdateOptions() { IsUpsert = true });
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassigned()
        {
            return await Query.Where(i => i.Assignees == null).ToListAsync();

        }

        public async Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.v", new List<string>() { userName });
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName)).ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassignedByUser(IEnumerable<string> roles)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterNotInRoles = builders.AnyNin("Assignees.k", roles);
            var result = await Collection.Find(builders.And(filterByTenantId, filterNotInRoles)).ToListAsync();

            return result.ToList();
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications(DateTimeOffset todayDate, List<string> excludedStatuses)
        {
            var tempData = Query.Where(p => !excludedStatuses.Contains(p.StatusCode)).ToList();
            IList<IFilterView> finalData = new  List<IFilterView>();
            foreach (var application in tempData)
            {
                if (application.ExpirationDate < todayDate)
                {
                    finalData.Add(application);
                }
            }
            return finalData;
            
        }
    }
}
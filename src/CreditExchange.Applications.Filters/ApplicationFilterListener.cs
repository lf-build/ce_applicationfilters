﻿using System;
using System.Linq;
using CreditExchange.Applicant;
using CreditExchange.Applicant.Client;
using CreditExchange.Application.Client;
using CreditExchange.ApplicationProcessor;
using CreditExchange.ApplicationProcessor.Client;
using CreditExchange.Applications.Filters.Abstractions.Configurations;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using Configuration = CreditExchange.Applications.Filters.Abstractions.Configurations.Configuration;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CreditExchange.Applications.Filters.Abstractions;
using LendFoundry.AssignmentEngine;
using LendFoundry.AssignmentEngine.Client;
using LendFoundry.Configuration;
using LendFoundry.Foundation.Date;

namespace CreditExchange.Applications.Filters
{
    public class ApplicationFilterListener : IApplicationFilterListener
    {
        public ApplicationFilterListener
            (
                IConfigurationServiceFactory<Configuration> apiConfigurationFactory,
                IConfigurationServiceFactory<TaggingConfiguration> apiTaggingConfigurationFactory,
                ITokenHandler tokenHandler,
                IEventHubClientFactory eventHubFactory,
                IFilterViewRepositoryFactory repositoryFactory,
                ILoggerFactory loggerFactory,
                ITenantServiceFactory tenantServiceFactory,
                IApplicationFilterServiceFactory applicationFilterServiceFactory,
                IApplicationServiceClientFactory applicationServiceFactory,
                IApplicantServiceClientFactory applicantServiceFactory,
                IStatusManagementServiceFactory statusManagementFactory,
                IConfigurationServiceFactory configurationFactory,
                ITenantTimeFactory tenantTimeFactory,
                IOfferEngineClientServiceFactory offerServiceFactory,
                IAssignmentServiceClientFactory assignmentFactory

            )
            {
                EventHubFactory = eventHubFactory;
                ApiConfigurationFactory = apiConfigurationFactory;
                TokenHandler = tokenHandler;
                RepositoryFactory = repositoryFactory;
                LoggerFactory = loggerFactory;
                TenantServiceFactory = tenantServiceFactory;
                ApplicationServiceFactory = applicationServiceFactory;
                ApplicantServiceFactory = applicantServiceFactory;
                ApplicationFilterServiceFactory = applicationFilterServiceFactory;
                StatusManagementFactory = statusManagementFactory;
                TenantTimeFactory = tenantTimeFactory;
                ConfigurationFactory = configurationFactory;
                ApiTaggingConfigurationFactory = apiTaggingConfigurationFactory;
                OfferServiceFactory = offerServiceFactory;
                AssignmentFactory = assignmentFactory;
            }

        private IStatusManagementServiceFactory StatusManagementFactory { get; }

        private IApplicationFilterServiceFactory ApplicationFilterServiceFactory { get; }

        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }

        private IApplicantServiceClientFactory ApplicantServiceFactory { get; }

        private IConfigurationServiceFactory<Configuration> ApiConfigurationFactory { get; }

        private IConfigurationServiceFactory<TaggingConfiguration> ApiTaggingConfigurationFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private ILoggerFactory LoggerFactory { get; }

        private ITenantTimeFactory TenantTimeFactory { get; }

        private IOfferEngineClientServiceFactory OfferServiceFactory { get; }

        private IAssignmentServiceClientFactory AssignmentFactory { get; }

        private static Object Key = new object ();

        public void Start ()
        {
            var logger = LoggerFactory.Create (NullLogContext.Instance);
            logger.Info ($"Eventhub listener started");

            try
            {
                var emptyReader = new StaticTokenReader (string.Empty);
                var tenantService = TenantServiceFactory.Create (emptyReader);
                var tenants = tenantService.GetActiveTenants ();
                logger.Info ($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach (tenant =>
                {
                    logger.Info ($"Initializing event hub for teanant #{tenant.Id}");
                    // Tenant token creation         
                    var token = TokenHandler.Issue (tenant.Id, "application-filters");
                    var reader = new StaticTokenReader (token.Value);

                    // Needed resources for this operation
                    var hub = EventHubFactory.Create (reader);
                    var statusManagementService = StatusManagementFactory.Create (reader);
                    var repository = RepositoryFactory.Create (reader);
                    var applicationService = ApplicationServiceFactory.Create (reader);
                    var applicantService = ApplicantServiceFactory.Create (reader);
                    var configuration = ApiConfigurationFactory.Create (reader).Get ();
                    var taggingConfiguration = ApiTaggingConfigurationFactory.Create (reader).Get ();
                    var tenantTime = TenantTimeFactory.Create (ConfigurationFactory, reader);
                    var offerService = OfferServiceFactory.Create (reader);
                    var assignmentService = AssignmentFactory.Create (reader);

                    if (configuration == null)
                        throw new ArgumentException ("Api configuration cannot be found, please check");

                    var applicationFilterService = ApplicationFilterServiceFactory.Create (reader, TokenHandler, logger);
                    // Attach all configured events to be listen
                    configuration
                        .Events
                        .ToList ()
                        .ForEach (eventConfig => hub.On (eventConfig.Name, AddView (eventConfig, repository, logger, applicationService, applicantService, statusManagementService, tenantTime, offerService, applicationFilterService, configuration, taggingConfiguration, assignmentService)));
                    hub.StartAsync ();
                });

            }
            catch (Exception ex)
            {
                logger.Error ("Error while listening eventhub", ex);
                Start ();
            }
        }

        private static Action<EventInfo> AddView (
            EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            Application.IApplicationService applicationService,
            IApplicantService applicantService,
            IEntityStatusService statusManagementService,
            ITenantTime tenantTime,
            IOfferEngineService offerService,
            IApplicationFilterServiceExtended applicationFilterService,
            Configuration configuration,
            TaggingConfiguration taggingConfiguration,
            IAssignmentService assignmentService
        )
        {
            return @event =>
            {
                try
                {
                    logger.Info ($"Now processing {@event.Name} with payload {@event.Data}");
                    // if (eventConfiguration.ShouldUpdateAppFilterRecord == true)
                    //  {
                    var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith (@event);
                    logger.Info ($"Processing {@event.Name} for Application Number : #{applicationNumber}");

                    var application = applicationService.GetByApplicationNumber (applicationNumber).Result;

                    if (application == null)
                    {
                        logger.Warn ($"No application found for application #{applicationNumber}");
                        return;
                    }

                    var applicant = applicantService.Get (application.ApplicantId).Result;

                    if (applicant == null)
                    {
                        logger.Warn ($"No applicants found for application #{applicationNumber}");
                        return;
                    }

                    // snapshot creation
                    var data = new FilterView ();

                    data.ApplicationId = application.ApplicantId;
                    data.ApplicantId = applicant.Id;
                    data.ApplicationNumber = application.ApplicationNumber;
                    data.Applicant = $"{applicant.FirstName} {applicant.MiddleName} {applicant.LastName}";
                    data.AmountRequested = application.RequestedAmount;
                    data.RequestTermType = application.RequestedTermType.ToString ();
                    data.RequestTermValue = application.RequestedTermValue;
                    data.ExpirationDate = application.ExpiryDate.Time;
                    data.ApplicantFirstName = applicant.FirstName;
                    data.ApplicantLastName = applicant.LastName;
                    data.ApplicantMiddleName = applicant.MiddleName;
                    data.PromoCode = application.PromoCode;
                    data.HunterStatus = application.HunterStatus;
                    data.SodexoCode = application.SodexoCode;
                    data.HunterId = application.HunterId;
                    data.SchemeCode = application.SchemeCode;
                    data.LeadSquaredCRMId = application.LeadSquaredCRMId;
                    if (application.Addresses != null && application.Addresses.Count > 0)
                    {
                        var currentAddress =
                            application.Addresses.FirstOrDefault (x => x.AddressType == AddressType.Current);
                        if (currentAddress != null)
                        {
                            data.ApplicantAddressLine1 = currentAddress.AddressLine1;
                            data.ApplicantAddressLine2 = currentAddress.AddressLine2;
                            data.ApplicantAddressLine3 = currentAddress.AddressLine3;
                            data.ApplicantAddressLine4 = currentAddress.AddressLine4;
                            data.ApplicantAddressCity = currentAddress.City;
                            data.ApplicantAddressCountry = currentAddress.Country;
                            data.ApplicantAddressIsDefault = currentAddress.IsDefault;
                            data.ApplicantAddressPincode = currentAddress.PinCode;
                        }
                    }

                    if (application.Source != null)
                    {
                        data.TrackingCode = application.Source.TrackingCode;
                        data.SourceId = application.Source.SourceReferenceId;
                        data.SourceType = application.Source.SourceType.ToString ();
                        data.TrackingCodeMedium = application.Source.TrackingCodeMedium;
                        data.SystemChannel = application.Source.SystemChannel.ToString ();
                        data.GCLId = application.Source.GCLId;
                        data.TrackingCodeCampaign = application.Source.TrackingCodeCampaign;
                        data.TrackingCodeTerm = application.Source.TrackingCodeTerm;
                        data.TrackingCodeContent = application.Source.TrackingCodeContent;
                    }

                    var applicationPhone =
                        application.PhoneNumbers?.FirstOrDefault (x => x.PhoneType == PhoneType.Mobile);
                    if (applicationPhone != null)
                    {
                        data.ApplicantPhone = applicationPhone.Phone;
                    }

                    data.ApplicantWorkEmail = applicant.EmploymentDetails != null ?
                        (applicant.EmploymentDetails.Any () ? applicant.EmploymentDetails[0].WorkEmail : string.Empty) :
                        string.Empty;

                    if (applicant.DateOfBirth != DateTimeOffset.MinValue)
                    {
                        data.ApplicantDateOfBirth = applicant.DateOfBirth;
                    }

                    if (application.EmailAddress != null)
                    {
                        data.ApplicantPersonalEmail = application.EmailAddress.Email;
                    }

                    data.ApplicantPanNumber = applicant.PermanentAccountNumber;
                    data.ApplicantAadharNumber = applicant.AadhaarNumber;

                    var employmentDetail = application.EmploymentDetail;
                    if (employmentDetail != null)
                    {
                        if (employmentDetail.IncomeInformation != null)
                            data.Income = application.EmploymentDetail.IncomeInformation.Income;
                        data.ApplicantEmployer = employmentDetail.Name;
                    }

                    data.Submitted = application.ApplicationDate.Time;

                    // gets all application assigness
                    var assignees = new Dictionary<string, string> ();
                    var assignments = assignmentService.Get ("Application", application.ApplicationNumber).Result;

                    if (assignments == null || assignments.Any () == false)
                        logger.Warn ($"No assignments found for this snapshot using EntityId#{application.ApplicationNumber}");

                    if (assignments != null && assignments.Any ())
                        assignees = assignments.ToDictionary (x => x.Role, x => x.Assignee);

                    data.Assignees = assignees;

                    var initialOffer = offerService.GetInitialOffer (applicationNumber).Result;
                    var selectedInitialOffer = initialOffer?.Offers?.Where (x => x.IsSelected == true).FirstOrDefault ();
                    if (selectedInitialOffer != null)
                    {
                        data.InitialOfferAmount = selectedInitialOffer.FinalOfferAmount;
                        data.InitialOfferInterestRate = selectedInitialOffer.InterestRate;
                        data.FileType = selectedInitialOffer.FileType;
                        data.InitialOfferLoanTenure = selectedInitialOffer.LoanTenure;
                        data.InitialOfferScore = selectedInitialOffer.Score;
                        if (eventConfiguration.ShouldUpdateInitialOfferSelectedDate == true)
                        {
                            data.InitialOfferSelectedOn = @event.Time.Time;
                        }
                    }

                    var finalOffer = offerService.GetFinalOffer (applicationNumber).Result;
                    var selectedFinalOffer = finalOffer?.FinalOffers?.Where (x => x.IsSelected == true).FirstOrDefault ();
                    if (selectedFinalOffer != null)
                    {
                        data.FinalOfferAmount = selectedFinalOffer.FinalOfferAmount;
                        data.FinalOfferInterestRate = selectedFinalOffer.InterestRate;
                        data.FinalOfferInstallmentAmount = selectedFinalOffer.Emi;
                        data.FinalOfferLoanTenure = selectedFinalOffer.Loantenure;
                        data.FinalOfferProcessingFee = selectedFinalOffer.ProcessingFee;
                        if (eventConfiguration.ShouldUpdateFinalOfferSelectedDate == true)
                        {
                            data.FinalOfferSelectedOn = @event.Time.Time;
                        }
                    }

                    var applicationStatus = statusManagementService.GetStatusByEntity ("application", applicationNumber).Result;
                    if (applicationStatus != null)
                    {
                        data.StatusCode = string.IsNullOrEmpty (applicationStatus.Code) ? string.Empty : applicationStatus.Code;
                        data.StatusName = applicationStatus.Name;
                        data.StatusDate = applicationStatus.ActiveOn.Time;
                        data.StatusReasons = applicationStatus.ReasonsSelected;
                        logger.Info ($"Status found for this snapshot #{data.StatusCode} #{data.StatusName}");

                        if (applicationStatus.Code.Equals (configuration.UpdateApprovedDateStatus))
                        {
                            data.ApprovedDate = applicationStatus.ActiveOn.Time;
                        }
                        else if (applicationStatus.Code.Equals (configuration.UpdateExpiredDateStatus))
                        {
                            data.OfferExpiredDate = applicationStatus.ActiveOn.Time;
                        }
                        else if (applicationStatus.Code.Equals (configuration.UpdateNotInterestedDateStatus))
                        {
                            data.NotInterestedDate = applicationStatus.ActiveOn.Time;
                        }
                        else if (applicationStatus.Code.Equals (configuration.UpdateRejectionDateStatus))
                        {
                            data.OfferRejectedDate = applicationStatus.ActiveOn.Time;
                        }
                    }
                    else
                        logger.Info ($"No status found for this snapshot");

                    if (eventConfiguration.UpdateLastProgressDate)
                    {
                        data.LastProgressDate = tenantTime.Today;
                    }

                    var fundDetails = applicationFilterService.GetDataAttributeForFunding ("application", applicationNumber).Result;
                    if (fundDetails != null)
                    {
                        data.DisbursedAmount = fundDetails.FundedAmount;
                        data.DisbursementDate = fundDetails.FundedDate;
                        data.EMIAmount = fundDetails.EmiAmount;
                        data.FirstEMIDate = fundDetails.FirstEmiDate;
                    }
                    if (!string.IsNullOrEmpty (data.Applicant))
                        data.LCaseApplicant = data.Applicant.ToLower ();
                    if (!string.IsNullOrEmpty (applicant.PermanentAccountNumber))
                        data.LCaseApplicantPanNumber = applicant.PermanentAccountNumber.ToLower ();
                    if (!string.IsNullOrEmpty (data.ApplicantEmployer))
                        data.LCaseApplicantEmployer = data.ApplicantEmployer.ToLower ();
                    if (!string.IsNullOrEmpty (data.ApplicantPersonalEmail))
                        data.LCaseApplicantPersonalEmail = data.ApplicantPersonalEmail.ToLower ();
                    // }
                    //Task.Run(() =>
                    //{
                    if (taggingConfiguration.Events.Exists (x => x.Name.ToLower () == eventConfiguration.Name.ToLower ()))
                    {
                        logger.Info ($"Tagging Method Started " + data.ApplicationNumber);
                        applicationFilterService.ProcessTaggingEvent (@event);
                        logger.Info ($"Tagging Method Completed " + data.ApplicationNumber);
                    }

                    var tagDetails = applicationFilterService.GetApplicationTagInformation (applicationNumber);
                    if (tagDetails != null)
                    {
                        data.Tags = tagDetails.Tags;
                        logger.Info ($"Tags are added to applicationFilters " + data.Tags + "ApplicationNumber :" + data.ApplicationNumber);
                    }

                    lock (Key)
                    {
                        repository.AddOrUpdate (data);
                    }
                    //});
                    logger.Info ($"New snapshot added to application {applicationNumber}");

                }
                catch (Exception ex) { logger.Error ($"Unhadled exception while listening event {@event.Name}", ex); }
            };
        }
    }
}
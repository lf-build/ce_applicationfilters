﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CreditExchange.Application;
using CreditExchange.Applications.Filters.Abstractions;
using CreditExchange.Applications.Filters.Abstractions.Configurations;
using CreditExchange.Applications.Filters.Abstractions.Services;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.DataAttributes;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.VerificationEngine;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CreditExchange.Applications.Filters
{
    public class ApplicationFilterService : IApplicationFilterServiceExtended
    {
        public ApplicationFilterService
            (
                IFilterViewRepository repository,
                IFilterViewTagsRepository tagsRepository,
                IEventHubClient eventHub,
                ILogger logger,
                ITenantTime tenantTime,
                Configuration configuration,
                TaggingConfiguration taggingConfiguration,
                IDecisionEngineService decisionEngineService,
                IEntityStatusService statusManagementServiceClient,
                IDataAttributesEngine dataAttributesEngine,
                IVerificationEngineService verificationEngineService,
                ICsvGenerator csvGenerator,
                ITokenReader tokenReader,
                ITokenHandler tokenParser,
                IIdentityService identityService,
                IApplicationService applicationservice
            )
            {
                Repository = repository;
                TagsRepository = tagsRepository;
                EventHub = eventHub;
                Logger = logger;
                TenantTime = tenantTime;
                Configuration = configuration;
                TaggingConfiguration = taggingConfiguration;
                DecisionEngineService = decisionEngineService;
                StatusManagementServiceClient = statusManagementServiceClient;
                DataAttributesEngine = dataAttributesEngine;
                VerificationEngineService = verificationEngineService;
                CsvGenerator = csvGenerator;
                TokenReader = tokenReader;
                TokenParser = tokenParser;
                IdentityService = identityService;
                ApplicationService = applicationservice;
            }
        private ITokenHandler TokenParser { get; }
        private ITokenReader TokenReader { get; }

        private IIdentityService IdentityService { get; }
        private Configuration Configuration { get; }

        private TaggingConfiguration TaggingConfiguration { get; }

        private IFilterViewRepository Repository { get; }

        private IFilterViewTagsRepository TagsRepository { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        private ITenantTime TenantTime { get; }

        private IDecisionEngineService DecisionEngineService { get; }

        private IEntityStatusService StatusManagementServiceClient { get; }

        private IDataAttributesEngine DataAttributesEngine { get; }

        private IVerificationEngineService VerificationEngineService { get; }
        private ICsvGenerator CsvGenerator { get; set; }

        private IApplicationService ApplicationService { get; }

        public IEnumerable<IFilterView> GetAll ()
        {
            return Repository.GetAll ();
        }

        public FileContentResult ExportAll (string templateName)
        {
            if (string.IsNullOrEmpty (templateName))
                throw new ArgumentException ($"{nameof(templateName)} is mandatory");

            var result = GetAll ();
            var templateDefinition = Configuration.ExportTemplates.FirstOrDefault (x => x.TemplateName == templateName);
            if (templateDefinition == null)
            {
                throw new Exception ("Missing template configuration.");
            }

            CsvGenerator.Delimiter = templateDefinition.Delimeter;
            return SendFileToClient (CsvGenerator.WriteToCsv (result, templateDefinition.CsvProjection), templateDefinition.FileNamePrefix + DateTime.Now.ToString ("dd-MM-yyyy") + ".csv");
        }

        public IEnumerable<IFilterView> GetAllBySource (string sourceType, string sourceId)
        {
            Validate (sourceType, sourceId);

            return Repository.GetAllBySource (sourceType, sourceId);
        }

        public IEnumerable<IFilterView> GetBySourceAndStatus (string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate (sourceType, sourceId);

            if (statuses == null || !statuses.Any ())
                throw new ArgumentException ($"{nameof(statuses)} is mandatory");

            return Repository.GetBySourceAndStatus (sourceType, sourceId, statuses);
        }

        public Task<int> GetTotalSubmittedForCurrentMonth (string sourceType, string sourceId)
        {
            try
            {
                Validate (sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset (new DateTime (TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentMonth (sourceType, sourceId, firstDayOfMonth);
            }
            catch (Exception ex)
            {
                Logger.Error ($"The method GetCountByMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public Task<int> GetTotalSubmittedForCurrentYear (string sourceType, string sourceId)
        {
            try
            {
                Validate (sourceType, sourceId);

                var firstDayOfYear = new DateTimeOffset (new DateTime (TenantTime.Now.Year, 1, 1, 0, 0, 0));
                return Repository.GetTotalSubmittedForCurrentYear (sourceType, sourceId, firstDayOfYear);
            }
            catch (Exception ex)
            {
                Logger.Error ($"The method GetCountByYear({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        private void Validate (string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace (sourceType))
                throw new InvalidArgumentException ($"{nameof(sourceType)} is mandatory", nameof (sourceType));

            if (string.IsNullOrWhiteSpace (sourceId))
                throw new InvalidArgumentException ($"{nameof(sourceId)} is mandatory", nameof (sourceId));
        }

        public Task<IEnumerable<IFilterView>> GetByName (string sourceType, string sourceId, string name)
        {
            try
            {
                Validate (sourceType, sourceId);

                if (string.IsNullOrWhiteSpace (name))
                    throw new InvalidArgumentException ($"{nameof(name)} is mandatory");

                return Repository.GetByName (sourceType, sourceId, name);
            }
            catch (Exception ex)
            {
                Logger.Error ($"The method GetByName({sourceType}, {sourceId}, {name}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        public Task<IEnumerable<IFilterView>> GetByStatus (string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate (sourceType, sourceId);

            if (statuses == null || !statuses.Any ())
                throw new ArgumentException ($"{nameof(statuses)} is mandatory");

            return Repository.GetByStatus (sourceType, sourceId, statuses);
        }

        public Task<IEnumerable<IFilterView>> GetAllByStatus (IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any ())
                throw new ArgumentException ($"{nameof(statuses)} is mandatory");

            return Repository.GetAllByStatus (statuses);
        }

        public async Task<FileContentResult> ExportAllByStatus (string templateName, IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any ())
                throw new ArgumentException ($"{nameof(statuses)} is mandatory");
            if (string.IsNullOrEmpty (templateName))
                throw new ArgumentException ($"{nameof(templateName)} is mandatory");

            var result = await Repository.GetAllByStatus (statuses);
            var templateDefinition = Configuration.ExportTemplates.FirstOrDefault (x => x.TemplateName == templateName);
            if (templateDefinition == null)
            {
                throw new Exception ("Missing template configuration.");
            }

            CsvGenerator.Delimiter = templateDefinition.Delimeter;
            return SendFileToClient (CsvGenerator.WriteToCsv (result, templateDefinition.CsvProjection), templateDefinition.FileNamePrefix + DateTime.Now.ToString ("dd-MM-yyyy") + ".csv");

        }

        public Task<int> GetCountByStatus (string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate (sourceType, sourceId);

            if (statuses == null || !statuses.Any ())
                throw new ArgumentException ($"{nameof(statuses)} is mandatory");

            return Repository.GetCountByStatus (sourceType, sourceId, statuses);
        }

        public async Task<IEnumerable<IFilterView>> GetAllByTag (IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any ())
                throw new ArgumentException ($"{nameof(tags)} is mandatory");

            IEnumerable<IFilterView> result = null;
            var taggedApplications = await TagsRepository.GetByTags (tags);

            if (taggedApplications != null)
            {
                var applicationNumbers = taggedApplications.Select (x => x.ApplicationNumber).ToList ();
                result = await Repository.All (x => applicationNumbers.Contains (x.ApplicationNumber));
            }

            return result;
        }

        public async Task<FileContentResult> ExportAllByTag (string templateName, IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any ())
                throw new ArgumentException ($"{nameof(tags)} is mandatory");

            if (string.IsNullOrEmpty (templateName))
                throw new ArgumentException ($"{nameof(templateName)} is mandatory");

            IEnumerable<IFilterView> result = null;
            var taggedApplications = await TagsRepository.GetByTags (tags);

            if (taggedApplications != null)
            {
                var applicationNumbers = taggedApplications.Select (x => x.ApplicationNumber).ToList ();
                result = await Repository.All (x => applicationNumbers.Contains (x.ApplicationNumber));
            }

            var templateDefinition = Configuration.ExportTemplates.FirstOrDefault (x => x.TemplateName == templateName);
            if (templateDefinition == null)
            {
                throw new Exception ("Missing template configuration.");
            }

            CsvGenerator.Delimiter = templateDefinition.Delimeter;
            return SendFileToClient (CsvGenerator.WriteToCsv (result, templateDefinition.CsvProjection), templateDefinition.FileNamePrefix + DateTime.Now.ToString ("dd-MM-yyyy") + ".csv");
        }

        public Task<int> GetCountByTag (IEnumerable<string> tags)
        {
            if (tags == null || !tags.Any ())
                throw new ArgumentException ($"{nameof(tags)} is mandatory");

            return TagsRepository.GetCountByTag (tags);
        }

        public double GetTotalAmountApprovedBySource (string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace (sourceType))
                throw new ArgumentException ($"{nameof(sourceType)} is mandatory");

            if (string.IsNullOrWhiteSpace (sourceId))
                throw new ArgumentException ($"{nameof(sourceId)} is mandatory");

            if (Configuration?.ApprovedStatuses == null || Configuration.ApprovedStatuses.Any () == false)
                throw new ArgumentException ($"{nameof(Configuration.ApprovedStatuses)} is mandatory in configuration");

            return
            Repository.GetTotalAmountApprovedBySourceAndStatus (sourceType, sourceId, Configuration.ApprovedStatuses);
        }

        public Task<IFilterView> GetByApplicationNumber (string sourceType, string sourceId, string applicationNumber)
        {
            Validate (sourceType, sourceId);

            if (string.IsNullOrWhiteSpace (applicationNumber))
                throw new InvalidArgumentException ($"{nameof(applicationNumber)} is mandatory", nameof (applicationNumber));

            var result = Repository.GetByApplicationNumber (sourceType, sourceId, applicationNumber);
            if (result.Result == null)
                throw new NotFoundException ($"The snapshot could not be found with SourceType:{sourceType}, SourceId:{sourceId}, ApplicationNumber:{applicationNumber}");

            return result;
        }

        public async Task<IFilterView> GetByApplicationNumberWithStatusHistory (string sourceType, string sourceId, string applicationNumber)
        {
            var filterDataView = await GetByApplicationNumber (sourceType, sourceId, applicationNumber);
            var statusHistory = StatusManagementServiceClient.GetStatusTransitionHistory ("application", applicationNumber).Result;
            filterDataView.StatusHistory = TransformHistoryToDictionary (statusHistory);
            return filterDataView;
        }

        public async Task<IFundingData> GetDataAttributeForFunding (string entityType, string entityId)
        {
            var fundDataDetails = await DataAttributesEngine.GetAttribute (entityType, entityId, "loan-fundeddata");
            FundingData fundingInfo = null;
            if (fundDataDetails != null)
            {
                return fundingInfo = JsonConvert.DeserializeObject<FundingData> (Convert.ToString (fundDataDetails));

            }
            return fundingInfo;
        }

        public async Task<IEnumerable<IFilterView>> DuplicateExistsData (UniqueParameterTypes parameterName, string parameterValue)
        {
            if (string.IsNullOrEmpty (parameterValue))
                throw new ArgumentNullException ($"required parameter {nameof(parameterValue)} missing");

            var application = await Repository.GetUsingUniqueAttributes (parameterName, parameterValue);
            return application;
        }

        public async Task<IEnumerable<IFilterView>> GetAllExpiredApplications ()
        {
            var todayDate = TenantTime.Today;
            return await Repository.GetAllExpiredApplications (todayDate);
        }

        public async Task<IEnumerable<IFilterView>> GetExcludedStatusExpired (IEnumerable<string> excludedStatuses)
        {
            if (excludedStatuses == null || !excludedStatuses.Any ())
                throw new ArgumentException ($"{nameof(excludedStatuses)} is mandatory");
            var todayDate = TenantTime.Today;
            return await Repository.GetAllExpiredApplications (todayDate, excludedStatuses.ToList ());
        }

        public async Task<IEnumerable<IFilterView>> SearchApplicationsForBank (ApplicationSearchParams searchParams, string condition)
        {
            IEnumerable<IFilterView> applicationSearchResult = null;
            if (string.IsNullOrEmpty (searchParams.StatusCode))
            {
                if (Configuration.BankPortalStatusCodes != null)
                {
                    if (Configuration.BankPortalStatusCodes.Length > 0)
                    {
                        searchParams.StatusCode = string.Join (",", Configuration.BankPortalStatusCodes.ToList ());
                        applicationSearchResult = await SearchApplications (searchParams, condition);
                    }
                }

            }
            else
            {
                if (Configuration.BankPortalStatusCodes != null)
                {
                    if (Configuration.BankPortalStatusCodes.Length > 0)
                    {
                        if (Configuration.BankPortalStatusCodes.Contains (searchParams.StatusCode))
                        {
                            applicationSearchResult = await SearchApplications (searchParams, condition);
                        }
                    }
                }
            }
            return applicationSearchResult;
        }

        public async Task<IEnumerable<IFilterView>> SearchApplications (ApplicationSearchParams searchParams, string condition)
        {
            IEnumerable<IFilterViewTags> applicationInTags = null, applicationNotInTags = null, tagSearchResults = null;
            IEnumerable<IFilterView> applicationSearchResult = Repository.SearchApplications (searchParams, condition);
            if (searchParams.InTags != null && searchParams.InTags.Count () > 0)
            {
                applicationInTags = await TagsRepository.GetByTags (searchParams.InTags);
            }

            if (searchParams.NotInTags != null && searchParams.NotInTags.Count () > 0)
            {
                applicationNotInTags = await TagsRepository.GetByNotInTags (searchParams.NotInTags);
            }

            if (!string.IsNullOrEmpty (condition) && condition.Equals ("and", StringComparison.OrdinalIgnoreCase))
            {
                tagSearchResults = applicationInTags != null ? (applicationNotInTags != null ? applicationInTags.Intersect (applicationNotInTags) : applicationInTags) : applicationNotInTags;
            }
            else
            {
                tagSearchResults = applicationInTags != null ? (applicationNotInTags != null ? applicationInTags.Union (applicationNotInTags) : applicationInTags) : applicationNotInTags;
            }

            if (tagSearchResults != null)
            {
                IEnumerable<string> additionalApplications;
                List<IFilterView> tempListOfApplications = new List<IFilterView> ();

                if (!string.IsNullOrEmpty (condition) && condition.Equals ("and", StringComparison.OrdinalIgnoreCase))
                {
                    if (applicationSearchResult != null)
                    {
                        applicationSearchResult = from application in applicationSearchResult
                        where tagSearchResults.Select (x => x.ApplicationNumber).Contains (application.ApplicationNumber)
                        select application;
                    }
                }
                else
                {
                    if (applicationSearchResult != null)
                    {
                        additionalApplications = from application in applicationSearchResult
                        where!tagSearchResults.Select (x => x.ApplicationNumber).Contains (application.ApplicationNumber)
                        select application.ApplicationNumber;
                        tempListOfApplications = applicationSearchResult.ToList ();
                    }
                    else
                    {
                        additionalApplications = tagSearchResults.Select (x => x.ApplicationNumber);
                    }

                    if (additionalApplications != null && additionalApplications.Count () > 0)
                    {
                        foreach (var applicationNumber in additionalApplications)
                        {
                            tempListOfApplications.Add (await Repository.GetByApplicationNumber (applicationNumber));
                        }
                    }
                    applicationSearchResult = tempListOfApplications;
                }
            }

            return applicationSearchResult;
        }

        public Tuple<string, FilterView, FilterViewTags> SearchApplicationForAttribute (string attributeName, string attributeValue)
        {
            if (string.IsNullOrEmpty (attributeName))
                throw new ArgumentException ("attributeName is null or empty string");
            if (string.IsNullOrEmpty (attributeValue))
                throw new ArgumentException ("attributeValue is null or empty string");

            var filteredApplication = Repository.SearchApplicationForAttribute (attributeName, attributeValue).OrderByDescending (x => x.Submitted).FirstOrDefault ();
            if (filteredApplication != null)
            {
                var filteredTagRecord = TagsRepository.GetTagsForApplication (filteredApplication.ApplicationNumber);

                var result = Tuple.Create (filteredApplication.ApplicationNumber, (FilterView) filteredApplication, (FilterViewTags) filteredTagRecord);
                return result;
            }
            else
            {
                return null;
            }
        }

        public void AttachFinacleDocumentID (string applicationNumber, string docManagerEntityId, string finacleDocumentId)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} can not be null");
            if (string.IsNullOrEmpty (finacleDocumentId))
                throw new ArgumentException ($"{nameof(finacleDocumentId)} can not be null");

            Repository.AttachFinacleDocumentId (applicationNumber, docManagerEntityId, finacleDocumentId);
        }

        public void AttachHunterDocumentID (string applicationNumber, string docManagerEntityId, string hunterDocumentId)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentException ($"{nameof(applicationNumber)} can not be null");
            if (string.IsNullOrEmpty (hunterDocumentId))
                throw new ArgumentException ($"{nameof(hunterDocumentId)} can not be null");

            Repository.AttachHunterDocumentId (applicationNumber, docManagerEntityId, hunterDocumentId);
        }

        public async Task<IEnumerable<IFilterView>> ApplicationsTaggedOn (string tagName, string fromDate, string toDate = null)
        {
            DateTimeOffset startDate, endDate;
            IEnumerable<string> appResult = null;
            if (string.IsNullOrEmpty (fromDate))
                throw new ArgumentNullException ($"parameter {nameof(fromDate)} is mandatory");

            var tagResult = await TagsRepository.GetByTags (new List<string> { tagName });
            startDate = DateTimeOffset.Parse (fromDate);
            if (!string.IsNullOrEmpty (toDate))
            {
                endDate = DateTimeOffset.Parse (toDate);
                appResult = from tagRecord in tagResult
                from tag in tagRecord.Tags
                where tag.TagName == tagName &&
                    (tag.TaggedOn.Date.CompareTo (startDate.Date) >= 0 && tag.TaggedOn.Date.CompareTo (endDate.Date) <= 0)
                select tagRecord.ApplicationNumber;
            }
            else
            {
                appResult = from tagRecord in tagResult
                from tag in tagRecord.Tags
                where tag.TagName == tagName && tag.TaggedOn.Date.CompareTo (startDate.Date) == 0
                select tagRecord.ApplicationNumber;
            }

            return await Repository.All (x => appResult.Contains (x.ApplicationNumber));
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassignedByUser ()
        {
            var roles = await GetUserRoles ();

            return await Repository.GetAllUnassignedByUser (roles);
        }

        private async Task<IEnumerable<string>> GetUserRoles ()
        {
            var userName = GetTokenUserName ();

            if (string.IsNullOrWhiteSpace (userName))
                throw new ArgumentException ("Token user cannot be found in the request, please verify");

            var roles = await IdentityService.GetUserRoles (userName);
            if (roles == null || roles.Any () == false)
                throw new NotFoundException ("Could not be found the roles from current user");

            return roles;
        }

        public async Task<IEnumerable<IFilterView>> GetAllUnassigned ()
        {

            return await Repository.GetAllUnassigned ();
        }

        private string GetTokenUserName ()
        {
            var token = TokenParser.Parse (TokenReader.Read ());
            return token.Subject;
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications ()
        {
            var userName = GetTokenUserName ();

            if (string.IsNullOrWhiteSpace (userName))
                throw new ArgumentException ("Token user cannot be found in the request, please verify");

            return Repository.GetAllAssignedApplications (userName);
        }

        #region "Tagging related methods"

        public async Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag (string tag)
        {
            return await TagsRepository.GetTagInfoForApplicationsByTag (tag);
        }

        public void TagsWithoutEvaluation (string applicationNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException ($"{nameof(applicationNumber)} is mandatory", nameof (applicationNumber));
            if (tags == null || tags.Count () == 0)
                throw new ArgumentNullException ($"{nameof(tags)} is mandatory", nameof (tags));

            ApplyTags (applicationNumber, tags);
        }

        public void UnTagWithoutEvaluation (string applicationNumber, IEnumerable<string> tags)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException ($"{nameof(applicationNumber)} is mandatory", nameof (applicationNumber));
            if (tags == null || tags.Count () == 0)
                throw new ArgumentNullException ($"{nameof(tags)} is mandatory", nameof (tags));

            RemoveTags (applicationNumber, tags);
        }

        public void ProcessTaggingEvent (EventInfo @event)
        {
            try
            {
                if (@event == null)
                    throw new InvalidArgumentException ($"#{nameof(@event)} cannot be null", nameof (@event));

                var taggingEventConfiguration = TaggingConfiguration.Events.FirstOrDefault (x => x.Name == @event.Name);

                if (taggingEventConfiguration == null)
                    throw new ArgumentException ($"Tagging event configuration for {@event.Name} not found");

                var applicationNumber = taggingEventConfiguration.ApplicationNumber.FormatWith (@event);
                Logger.Info ($"Processing tagging event {@event.Name} for application {applicationNumber}");

                ApplyTags (applicationNumber, taggingEventConfiguration.ApplyTags);
                RemoveTags (applicationNumber, taggingEventConfiguration.RemoveTags);

                taggingEventConfiguration.Rules?.ToList ().ForEach (x =>
                {
                    try
                    {
                        var dataAttributes = DataAttributesEngine.GetAllAttributes ("application", applicationNumber).Result;
                        var verificationDetails = VerificationEngineService.GetFactVerification ("application", applicationNumber).Result;
                        var applicationStatusDetails = StatusManagementServiceClient.GetStatusByEntity ("application", applicationNumber).Result;
                        var input = new { Data = @event.Data, DataAttributes = dataAttributes, VerificationDetails = verificationDetails, ApplicationStatus = applicationStatusDetails };
                        var ruleName = x.Name;
                        var version = x.Version;

                        var ruleResult = DecisionEngineService.Execute<dynamic, RuleResult> (ruleName, new { payload = input });
                        Logger.Info ($"{ruleResult.ApplyTags} to be applied to application {applicationNumber}");
                        Logger.Info ($"{ruleResult.RemoveTags} to be removed from application {applicationNumber}");
                        if (ruleResult != null)
                        {
                            ApplyTags (applicationNumber, ruleResult.ApplyTags);
                            RemoveTags (applicationNumber, ruleResult.RemoveTags);
                        }

                    }
                    catch (Exception ex)
                    {
                        Logger.Error ($"Unhandled exception while processing application #{applicationNumber} for rule #{x.Name}.", ex);
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error ($"The method ProcessTaggingEvent({@event}) raised an error:{ex.Message}, Full Error:", ex);
            }
        }

        public IFilterViewTags GetApplicationTagInformation (string applicationNumber)
        {
            if (string.IsNullOrEmpty (applicationNumber))
                throw new ArgumentNullException ("Application Number required to fetch tag information");
            return TagsRepository.GetTagsForApplication (applicationNumber);
        }

        private void ApplyTags (string applicationNumber, IEnumerable<string> tags)
        {
            tags?.ToList ().ForEach (x =>
            {
                if (!string.IsNullOrEmpty (x))
                {
                    TagsRepository.AddTag (applicationNumber, x);
                    EventHub.Publish ("TagApplied", new { ApplicationNumber = applicationNumber, TagName = x });
                }
            });
        }

        private void RemoveTags (string applicationNumber, IEnumerable<string> tags)
        {
            tags?.ToList ().ForEach (x =>
            {
                if (!string.IsNullOrEmpty (x))
                {
                    if (x == "ALL")
                    {
                        RemoveAllTags (applicationNumber);
                        return;
                    }
                    TagsRepository.RemoveTag (applicationNumber, x);
                    EventHub.Publish ("TagRemoved", new { ApplicationNumber = applicationNumber, TagName = x });
                }
            });
        }

        private void RemoveAllTags (string applicationNumber)
        {
            TagsRepository.ClearAllTags (applicationNumber);
        }

        #endregion

        private Dictionary<string, object> TransformHistoryToDictionary (IEnumerable<IEntityStatus> statusHistory)
        {
            Dictionary<string, object> statusHistoryDictionary = null;
            if (statusHistory != null && statusHistory.Count () > 0)
            {
                statusHistoryDictionary = new Dictionary<string, object> ();
                foreach (var status in statusHistory)
                {
                    StringBuilder strReason = new StringBuilder ();
                    if (status.Reason != null && status.Reason.Count > 0)
                    {
                        foreach (var reason in status.Reason)
                        {
                            strReason.Append (reason + ",");
                        }
                    }

                    statusHistoryDictionary.Add (status.Id, new
                    {
                        Status = status.Status,
                            ActivatedBy = status.ActivatedBy,
                            ActiveOn = status.ActivedOn,
                            Reasons = strReason.Length > 0 ? strReason.ToString ().Substring (0, strReason.Length - 1) : string.Empty
                    });
                }
            }
            return statusHistoryDictionary;
        }

        private FileContentResult SendFileToClient (byte[] exportStream, string fileName)
        {
            FileContentResult result = new FileContentResult (exportStream, "application/octet-stream")
            {
                FileDownloadName = fileName
            };
            return result;
        }

        public async Task<IDictionary<string, int>> GetReportStatusCount ()
        {
            return await Repository.GetReportStatusCount ();
        }

        public async Task<IEnumerable<IFilterView>> GetUTMTagReport ()
        {
            try
            {
                List<IApplication> listofapplication = await ApplicationService.GetApplicationsWithTrackingCode ();

                var listofApplicationNumber = listofapplication.Select (x => x.ApplicationNumber).ToList ();
                var result = Repository.GetUTMTagReport (listofApplicationNumber).ToList ();

                result.ForEach (x =>
                {
                    var trackingInfo = listofapplication.FirstOrDefault (z => string.Compare (x.ApplicationNumber, z.ApplicationNumber) == 0);
                    if (trackingInfo != null)
                    {
                        x.TrackingCode = trackingInfo.Source.TrackingCode;
                    }
                });

                return result;

            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}